﻿using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using EshopWebApp.Models;
using Newtonsoft.Json;
using System.Text;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace EshopWebApp.Tests
{
    public class EshopTests : IClassFixture<WebApplicationFactory<EshopWebApp.Startup>>
    {
        public HttpClient Client { get; }
        private readonly WebApplicationFactory<Startup> _factory;

        public EshopTests(WebApplicationFactory<EshopWebApp.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Test_PostData()
        {
            var client = _factory.CreateClient();
            var response = await client.PostAsync("api/v1.0/eshopitemscontroller/products"
                , new StringContent(
                JsonConvert.SerializeObject(new EshopItem()
                {
                    Name = "Test",
                    Description = "John",
                    Price = 10,
                    ImgUri = "api"
                }),
                Encoding.UTF8,
                "application/json"));

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task Test_GetData()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("api/v1.0/eshopitemscontroller/products");
            var itemsAfter = JsonConvert.DeserializeObject<EshopItem[]>(await response.Content.ReadAsStringAsync());
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.NotEmpty(itemsAfter);
        }

        [Fact]
        public async Task Test_PutData()
        {
            var client = _factory.CreateClient();

            var responseBefore = await client.PutAsync("api/v1.0/eshopitemscontroller/products/1"
                , new StringContent(
                JsonConvert.SerializeObject(new EshopItem()
                {
                    ID = 1,
                    Name = "Test_mod",
                    Description = "Modified",
                    Price = 999,
                    ImgUri = "api_mod",
                }),
                Encoding.UTF8,
                "application/json"));

            Assert.Equal(HttpStatusCode.OK, responseBefore.StatusCode);

            var responseAfter = await client.GetAsync("api/v1.0/eshopitemscontroller/products/1");
            var itemsAfter = JsonConvert.DeserializeObject<EshopItem>(await responseAfter.Content.ReadAsStringAsync());
            Assert.Equal(HttpStatusCode.OK, responseAfter.StatusCode);
            Assert.Equal("Modified", itemsAfter.Description);
            Assert.Equal("api_mod", itemsAfter.ImgUri);
            Assert.Equal(999, itemsAfter.Price);
            Assert.Equal("Test_mod", itemsAfter.Name);
        }

        [Fact]
        public async Task Put_Description()
        {
            var client = _factory.CreateClient();

            var responseBefore = await client.PutAsync("api/v1.0/eshopitemscontroller/products/description/1"
                , new StringContent(
                JsonConvert.SerializeObject(new DesciptionItem()
                {
                    ID = 1,
                    Description = "Changed!",
                }),
                Encoding.UTF8,
                "application/json"));

            Assert.Equal(HttpStatusCode.OK, responseBefore.StatusCode);

            var responseAfter = await client.GetAsync("api/v1.0/eshopitemscontroller/products/1");
            var itemsAfter = JsonConvert.DeserializeObject<EshopItem>(await responseAfter.Content.ReadAsStringAsync());
            Assert.Equal(HttpStatusCode.OK, responseAfter.StatusCode);
            Assert.Equal("Changed!", itemsAfter.Description);
        }

        [Fact]
        public async Task Test_GetData_Paged()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("api/v2.0/eshopitemscontroller/productsPaged?PageNumber=2");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task Test_GetData_ByID()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("api/v1.0/eshopitemscontroller/products/1");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task Test_DeleteData()
        {
            var client = _factory.CreateClient();

            var responseBefore = await client.GetAsync("api/v1.0/eshopitemscontroller/products");
            var itemsBefore = JsonConvert.DeserializeObject<EshopItem[]>(await responseBefore.Content.ReadAsStringAsync());
            Assert.Equal(HttpStatusCode.OK, responseBefore.StatusCode);

            var responseDel = await client.DeleteAsync("api/v1.0/eshopitemscontroller/products/" + itemsBefore.Length);
            Assert.Equal(HttpStatusCode.OK, responseDel.StatusCode);

            var responseAfter = await client.GetAsync("api/v1.0/eshopitemscontroller/products");
            var itemsAfter = JsonConvert.DeserializeObject<EshopItem[]>(await responseAfter.Content.ReadAsStringAsync());
            Assert.Equal(HttpStatusCode.OK, responseAfter.StatusCode);
            Assert.Equal(itemsAfter.Length, itemsBefore.Length - 1);
        }
    }
}
