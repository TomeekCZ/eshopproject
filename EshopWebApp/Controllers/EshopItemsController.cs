﻿using EshopWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace EshopWebApp.Controllers
{

    [Route("api/v{version:apiVersion}/eshopitemscontroller")]
    [Produces("application/json")]
    [ApiController]
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    public class EshopItemsController : ControllerBase
    {
        private readonly EshopContext _context;

        public EshopItemsController(EshopContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all the values from the database
        /// </summary>
        /// <response code="200">Returns item</response>
        /// <response code="400">If the request is not correctly formed</response>
        /// <response code="404">If the resource is not found on the server</response>
        [HttpGet("products")]
        [MapToApiVersion("1.0")]
        public async Task<ActionResult<IEnumerable<EshopItem>>> GetEshopItem()
        {
            return await _context.EshopITem.ToListAsync();
        }

        /// <summary>
        /// Gets all the values, but paged (default page size is 10)
        /// </summary>
        /// <response code="200">Returns item</response>
        /// <response code="400">If the request is not correctly formed</response>
        /// <response code="404">If the resource is not found on the server</response>
        /// <param name="eshopParameters"></param>
        /// <returns></returns>
        [HttpGet("productsPaged")]
        [MapToApiVersion("2.0")]
        public async Task<ActionResult<IEnumerable<EshopItem>>> GetEshopItemPerPage([FromQuery] EshopParameters eshopParameters)
        {
            return await _context.EshopITem.Skip((eshopParameters.PageNumber - 1) * eshopParameters.PageSize)
               .Take(eshopParameters.PageSize).ToListAsync();
        }

        /// <summary>
        /// Gets a value specified by its ID
        /// </summary>
        /// <response code="200">Returns item</response>
        /// <response code="400">If the request is not correctly formed</response>
        /// <response code="404">If the resource is not found on the server</response>
        /// <param name="id">ID value of the item</param>
        /// <returns></returns>
        [HttpGet("products/{id}")]
        public async Task<ActionResult<EshopItem>> GetEshopItem(int id)
        {
            var eshopItem = await _context.EshopITem.FindAsync(id);

            if (eshopItem == null)
            {
                return NotFound();
            }

            return eshopItem;
        }

        /// <summary>
        /// Changes value of the item specified by its ID value
        /// </summary>
        /// <response code="200">Returns changed item</response>
        /// <response code="400">If the request is not correctly formed</response>
        /// <response code="404">If the resource is not found on the server</response>
        /// <param name="id">ID value of the item</param>
        /// <param name="eshopItem">item of EshopItem class to be added</param>
        /// <returns></returns>
        [HttpPut("products/{id}")]
        public async Task<ActionResult<EshopItem>> PutEshopItem(int id, EshopItem eshopItem)
        {
            if (id != eshopItem.ID)
            {
                return BadRequest();
            }

            _context.Entry(eshopItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!EshopItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return eshopItem;
        }

        /// <summary>
        /// /// Changes the description of the item specified by its ID value
        /// </summary>
        /// <response code="200">Returns changed item</response>
        /// <response code="400">If the request is not correctly formed</response>
        /// <response code="404">If the resource is not found on the server</response>
        /// <param name="id">ID value of the item</param>
        /// <param name="descriptionItem"></param>
        /// <returns></returns>
        [HttpPut("products/description/{id}")]
        public async Task<ActionResult<EshopItem>> PutEshopDescription(int id, DesciptionItem descriptionItem)
        {
            if (id != descriptionItem.ID)
            {
                return BadRequest();
            }

            var eshopItem = await _context.EshopITem.FindAsync(id);
            eshopItem.Description = descriptionItem.Description;

            _context.Entry(eshopItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!EshopItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return eshopItem;
        }

        /// <summary>
        /// Posts new item into the database
        /// </summary>
        /// <response code="201">Returns inserted item</response>
        /// <response code="400">If the request is not correctly formed</response>
        /// <response code="404">If the resource is not found on the server</response>
        /// <param name="eshopItem">item of EshopItem class to be added</param>
        /// <returns></returns>
        [HttpPost("products")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<EshopItem>> PostEshopItem(EshopItem eshopItem)
        {
            _context.EshopITem.Add(eshopItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEshopItem", new { id = eshopItem.ID }, eshopItem);
        }

        /// <summary>
        /// Deletes item with specific ID
        /// </summary>
        /// <param name="id">ID value of the item</param>
        /// <returns></returns>
        [HttpDelete("products/{id}")]
        public async Task<ActionResult<EshopItem>> DeleteEshopItem(int id)
        {
            var eshopItem = await _context.EshopITem.FindAsync(id);
            if (eshopItem == null)
            {
                return NotFound();
            }

            _context.EshopITem.Remove(eshopItem);
            await _context.SaveChangesAsync();

            return eshopItem;
        }

        private bool EshopItemExists(int id)
        {
            return _context.EshopITem.Any(e => e.ID == id);
        }
    }
}
