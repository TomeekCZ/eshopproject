﻿using System.ComponentModel.DataAnnotations;

namespace EshopWebApp.Models
{
    public class EshopItem
    {
        [Required]
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ImgUri { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string Description { get; set; }
    }
}
