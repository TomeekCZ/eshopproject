﻿using Microsoft.EntityFrameworkCore;

namespace EshopWebApp.Models
{
    public class EshopContext : DbContext
    {
        public EshopContext(DbContextOptions<EshopContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<EshopItem>()
                .Property(p => p.Price)
                .HasColumnType("decimal(20,4)");
        }

        public DbSet<EshopItem> EshopITem { get; set; }
    }
}
