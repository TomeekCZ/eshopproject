# Simple Eshop REST API

This README file describes how to get this web application up and running

## Prerequisities

* Visual Studio Community ! [https://visualstudio.microsoft.com/vs/community/](https://visualstudio.microsoft.com/vs/community/)
* Git bash or similar tool ! [https://git-scm.com/downloads](https://git-scm.com/downloads)

## Source Code

Source code is available on Bitbucket public git repository :

> [https://bitbucket.org/TomeekCZ/eshopproject/src/master/](https://bitbucket.org/TomeekCZ/eshopproject/src/master/)

There is also public Jira with all the tasks done when creating this project

> [https://tom-n-me-ek.atlassian.net/jira/software/c/projects/ES/issues](https://tom-n-me-ek.atlassian.net/jira/software/c/projects/ES/issues)

To dowload it:
* Install Git Bash on your machine
* After running the application, locate the folder to which you want the content to be dowlonaded
* Run following line of code

```
git clone https://TomeekCZ@bitbucket.org/TomeekCZ/eshopproject.git
```

* Using File Explorer navigate to the project folder, locate file __EshopWebApp.sln__ and open it in Visual Studio.
* From menu, open __Tools > NuGet Package Manager > Package Manager Console__
* Run following command, which will create local database, which is later used for storing production data within the application

```
Update-Database
```

* Build the application and run it, you will be greated with a homepage of Swagger documentation describing all the endpoints this API offers

## Running Unit Tests

* To run unit test within Visual Studio we need to make one change - we need to switch between test database and production database. This setting is stored in __appsettings.json__ file within Visual Studio, but you can modify it also in File Explorer.

```json
"TestDBSettings": {
    "TestDBUsed": "false"
}
```

* To tell the application to use test database, simply change the value to __true__. Once finished testing, you need to switch the database back to production.

* To run all the unit test, open Test Explorer Windows in Visual Studio - __Test > Test Explorer__ or __Ctrl+E, T__ and hit __Run All Tests__ (NOTE: if you leave the production database running, most test will fail, because most of them expect some data to be in the database already)

